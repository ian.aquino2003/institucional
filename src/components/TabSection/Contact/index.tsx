import React from "react";
import { Field, Form, Formik, FormikValues } from "formik";
import Input from "./Input";
import * as yup from "yup";
import "yup-phone";
import parse from "date-fns/parse";
import { cpf as cpfValidator } from "cpf-cnpj-validator";

import styles from "./Contact.module.css";

export default function About() {
  const today = new Date();

  const createUserFormSchema = yup.object().shape({
    name: yup
      .string()
      .min(3, "Nome pequeno demais")
      .required("Nome obrigatório!"),

    email: yup
      .string()
      .required("E-mail obrigatório!")
      .email("E-mail obrigatório!"),

    cpf: yup
      .string()
      .test("999.999.999-99", "CPF inválido!", (val = "") =>
        cpfValidator.isValid(val)
      )
      .required("CPF obrigatório!"),

    birthdate: yup
      .date()
      .transform(function (value, originalValue) {
        if (this.isType(value)) {
          return value;
        }
        const result = parse(originalValue, "dd.MM.yyyy", new Date());
        return result;
      })
      .typeError("Data inválida!")
      .required("Data de nascimento é obrigatório!")
      .min("1969-11-13", "Data muito antiga")
      .max(today, "Sua data está no futuro!"),

    telefone: yup
      .string()
      .phone("BR", true, "Telefone inválido!")
      .required("Telefone obrigatório!"),

    instagram: yup
      .string()
      .max(30, "Limite de caracteres excedido")
      .required("Instagram obrigatório!"),

    agreement: yup.bool().oneOf([true], "É obrigatório!"),
  });

  function handleSubmitForm(values: FormikValues, evt: FormikValues) {
    console.log(values);
    evt.resetForm();
  }

  return (
    <div className={styles.container}>
      <Formik
        initialValues={{
          name: "",
          email: "",
          cpf: "",
          birthdate: "",
          telefone: "",
          instagram: "",
          agreement: false,
        }}
        onSubmit={handleSubmitForm}
        validationSchema={createUserFormSchema}
      >
        {({ errors }) => (
          <Form action="">
            <h2>PREENCHA O FORMULÁRIO</h2>
            <Input
              label="Nome"
              placeholder="Seu nome completo"
              title="name"
              error={errors.name}
              mask={""}
            />
            <Input
              label="E-mail"
              placeholder="Seu e-mail"
              title="email"
              error={errors.email}
              mask={""}
            />
            <Input
              label="CPF"
              placeholder="000 000 000 00"
              title="cpf"
              error={errors.cpf}
              mask={""}
            />
            <Input
              label="Data de Nascimento:"
              placeholder="00 . 00 . 0000"
              title="birthdate"
              error={errors.birthdate}
              mask={""}
            />
            <Input
              label="Telefone:"
              placeholder="(+00) 00000 0000"
              title="telefone"
              error={errors.telefone}
              mask={""}
            />
            <Input
              label="Instagram"
              placeholder="@seuuser"
              title="instagram"
              error={errors.instagram}
              mask={""}
            />

            <div className={styles.agreement}>
              <label htmlFor="agreement">
                <span>*</span>
                Declare que li e aceito
              </label>
              <Field type="checkbox" id="agreement" name="agreement" />
              {errors.agreement ? <p>{errors.agreement}</p> : null}
            </div>

            <button type="submit">CADASTRE-SE</button>
          </Form>
        )}
      </Formik>
    </div>
  );
}
