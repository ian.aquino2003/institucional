import React from "react";

import {
  Boleto,
  DevelopedBy,
  Diners,
  Elo,
  Hiper,
  Master,
  Pagseguro,
  PoweredBy,
  Visa,
  VtexCertified,
} from "./ImagesImports";

import useWindowsDimensions from "hooks/useWindowsDimensions";

import styles from "./Footer.module.css";
import ContactInfos from "./ContactInfos";

export default function Footer() {
  const [width] = useWindowsDimensions();

  return (
    <>
      <ContactInfos />
      <footer className={styles.container}>
        <div>
          Lorem ipsum dolor sit amet, consectetur adipiscing <br /> elit
          {width >= 768 ? <>, sed do eiusmod tempor</> : <>. .</>}
        </div>
        <div className={styles.cards}>
          <img src={Boleto} alt="Boleto" />
          <img src={Diners} alt="Diners" />
          <img src={Elo} alt="Elo" />
          <img src={Hiper} alt="Hiper" />
          <img src={Master} alt="Master" />
          <img src={Pagseguro} alt="Pagseguro" />
          <img src={Visa} alt="Visa" />
          <img src={VtexCertified} alt="VTEX certified" />
        </div>
        <div className={styles.lastInfos}>
          <a href="https://vtex.com/br-pt/">
            <p>Powered by</p>
            <img src={PoweredBy} alt="VTEX" />
          </a>
          <a href="https://m3ecommerce.com/">
            <p>Developed by</p>
            <img src={DevelopedBy} alt="M3" />
          </a>
        </div>
      </footer>
    </>
  );
}
