import React from "react";

import "./global.css";
import Institucional from "./pages/Institucional";

function App() {
  return (
    <div className="App">
      <Institucional />
    </div>
  );
}

export default App;
