import React from "react";
import MagnifyGlass from "assets/img/magnifying-glass.svg";

import styles from "./Search.module.css";

export default function SearchInput() {
  return (
    <>
      <div className={styles.container}>
        <input type="text" id="search" placeholder="Buscar..." />
        <label htmlFor="search">
          <img src={MagnifyGlass} alt="Magnify glass for search" />
        </label>
      </div>
    </>
  );
}
