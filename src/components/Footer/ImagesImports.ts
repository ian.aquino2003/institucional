import PoweredBy from "assets/img/footer/vtex.svg";
import DevelopedBy from "assets/img/footer/m3.svg";
import VtexCertified from "assets/img/footer/vtex-pci.png";
import Boleto from "assets/img/footer/Boleto.png";
import Diners from "assets/img/footer/Diners.png";
import Elo from "assets/img/footer/Elo.png";
import Hiper from "assets/img/footer/Hiper.png";
import Master from "assets/img/footer/Master.png";
import Pagseguro from "assets/img/footer/Pagseguro.png";
import Visa from "assets/img/footer/Visa.png";

export {
  PoweredBy,
  DevelopedBy,
  VtexCertified,
  Boleto,
  Diners,
  Elo,
  Hiper,
  Master,
  Pagseguro,
  Visa,
};
