import React, { useContext } from "react";
import { TabsContext } from "contexts/TabsProvider";

import styles from "./TabButton.module.css";

interface TabButtonProps {
  name: string;
}

export default function TabButton({ name }: TabButtonProps) {
  const { actualContent, setContent } = useContext(TabsContext);

  return (
    <li
      className={
        actualContent === name
          ? styles.container + " " + styles.active
          : styles.container
      }
      onClick={() => setContent(name)}
    >
      <p>{name}</p>
    </li>
  );
}
