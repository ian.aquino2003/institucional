import React from "react";

import styles from "./Navigation.module.css";

export default function Navigation() {
  return (
    <nav className={styles.container}>
      <button>CURSOS</button>
      <button>SAIBA MAIS</button>
    </nav>
  );
}
