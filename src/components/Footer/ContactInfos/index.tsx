import React from "react";

import Facebook from "assets/img/footer/facebook.svg";
import Instagram from "assets/img/footer/instagram.svg";
import Twitter from "assets/img/footer/twitter.svg";
import Youtube from "assets/img/footer/youtube.svg";
import Linkedin from "assets/img/footer/linkedin.svg";

import useWindowsDimensions from "hooks/useWindowsDimensions";

import styles from "./ContactInfos.module.css";

export default function ContactInfos() {
  const [width] = useWindowsDimensions();

  return (
    <>
      <div className={styles.container}>
        <nav>
          <h2>Institucional</h2>
          {width >= 768 ? (
            <>
              <p>Quem Somos</p>
              <p>Política de Privacidade</p>
              <p>Segurança</p>
              <p className={styles.active}>Seja um Revendedor</p>
            </>
          ) : (
            <p>+</p>
          )}
        </nav>
        <nav>
          <h2>Dúvidas</h2>
          {width >= 768 ? (
            <>
              <p>Entrega</p>
              <p>Pagamento</p>
              <p>Trocas e Devoluções</p>
              <p>Dúvidas Frequentes</p>
            </>
          ) : (
            <p>+</p>
          )}
        </nav>
        <nav>
          <h2>Fale Conosco</h2>
          {width >= 768 ? (
            <>
              <b>Atendimento ao Consumidor</b>
              <p>(11) 4159 9504</p>
              <b>Atendimento Online</b>
              <p>(11) 99433-8825</p>
            </>
          ) : (
            <p>+</p>
          )}
        </nav>

        <section className={styles.socialSection}>
          <picture>
            <img src={Facebook} alt="Facebook" />
          </picture>
          <picture>
            <img src={Instagram} alt="Instagram" />
          </picture>
          <picture>
            <img src={Twitter} alt="Twitter" />
          </picture>
          <picture>
            <img src={Youtube} alt="Youtube" />
          </picture>
          <picture>
            <img src={Linkedin} alt="Linkedin" />
          </picture>
          {width >= 768 && <p>www.loremipsum.com</p>}
        </section>
      </div>
    </>
  );
}
