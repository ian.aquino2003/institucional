import React, { createContext, ReactNode, useState } from "react";

interface TabsContextValue {
  actualContent: string;
  setContent: (name: string) => void;
}

interface TabsProviderProps {
  children: ReactNode;
}

export const TabsContext = createContext({} as TabsContextValue);

export default function TabsProvider({ children }: TabsProviderProps) {
  const [actualContent, setActualContent] = useState("Sobre");

  function setContent(name: string) {
    setActualContent(name);
  }

  return (
    <TabsContext.Provider value={{ actualContent, setContent }}>
      {children}
    </TabsContext.Provider>
  );
}
