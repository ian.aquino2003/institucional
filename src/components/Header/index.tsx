import React, { useEffect, useState } from "react";

import CartSVG from "assets/img/cart.svg";
import LogoSVG from "assets/img/logo.svg";
import HamburguerSVG from "assets/img/hamburguer.svg";

import SearchInput from "./SearchInput";

import useWindowsDimensions from "hooks/useWindowsDimensions";

import Navigation from "./Navigation";
import styles from "./Header.module.css";
import SideBar from "./SideBar";

export default function Header() {
  const [width] = useWindowsDimensions();
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {}, [isOpen]);

  return (
    <>
      <header className={styles.container}>
        {width <= 768 && (
          <button
            type="button"
            className={styles.hamburguer}
            onClick={() => setIsOpen(true)}
          >
            <img src={HamburguerSVG} alt="Hamburguer menu icon" />
          </button>
        )}
        <SideBar isOpen={isOpen} setIsOpen={setIsOpen} />

        <img src={LogoSVG} alt="Logo M3 Academy" />
        <SearchInput />
        <div className={styles.loggedInfos}>
          {width >= 768 && <button>entrar</button>}
          <img src={CartSVG} alt="Cart Icon" />
        </div>
      </header>
      {width > 768 && <Navigation />}
    </>
  );
}
