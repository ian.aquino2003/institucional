import React from "react";
import Footer from "components/Footer";
import Header from "components/Header";
import TabsProvider from "contexts/TabsProvider";

import MainContent from "components/TabSection";
import Newsletter from "components/Newsletter";
import PathToPage from "components/PathToPage";

import styles from "./Institucional.module.css";
import Whatsapp from "components/Whatsapp";
import ScrollUp from "components/ScrollUp";

export default function Institucional() {
  return (
    <>
      <TabsProvider>
        <Header />
        <Whatsapp />
        <ScrollUp />
        <PathToPage />
        <div className={styles.pageTitle}>
          <p>INSTITUCIONAL</p>
        </div>

        <MainContent />

        <Newsletter />
        <Footer />
      </TabsProvider>
    </>
  );
}
