import { useEffect, useState } from "react";

export default function useWindowsDimensions() {
  const [windowSize, setWindowSize] = useState({
    width: 0,
    height: 0,
  });

  useEffect(() => {
    function ResizeWindow() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    window.addEventListener("resize", ResizeWindow);
    ResizeWindow();
    return () => window.removeEventListener("resize", ResizeWindow);
  }, []);
  return [windowSize.width, windowSize.height];
}
