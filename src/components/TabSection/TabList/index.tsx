import React from "react";
import TabButton from "./TabButton";

import styles from "./TabList.module.css";

export default function TabList() {
  return (
    <ul className={styles.container}>
      <TabButton name="Sobre" />
      <TabButton name="Forma de Pagamento" />
      <TabButton name="Entrega" />
      <TabButton name="Troca e Devolução" />
      <TabButton name="Segurança e Privacidade" />
      <TabButton name="Contato" />
    </ul>
  );
}
