import { Field } from "formik";
import React from "react";
import styles from "./Input.module.css";

interface InputProps {
  title: string;
  label: string;
  placeholder: string;
  mask: string;
  error: string | undefined;
}

export default function Input({
  title,
  label,
  placeholder,
  mask,
  error,
}: InputProps) {
  return (
    <div className={styles.container}>
      <label htmlFor={title}>
        {label}
        {error ? <p>{error}</p> : null}
      </label>
      <Field id={title} type="text" name={title} placeholder={placeholder} />
    </div>
  );
}
