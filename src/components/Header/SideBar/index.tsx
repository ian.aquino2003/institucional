import React, { Dispatch, SetStateAction } from "react";

import styles from "./Sidebar.module.css";

interface SideBarProps {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
}

export default function SideBar({ setIsOpen, isOpen }: SideBarProps) {
  return (
    <div
      className={`${styles.container} ${isOpen && styles.open}`}
      onClick={() => setIsOpen(false)}
    >
      <aside
        onClick={(evt) => {
          evt.stopPropagation();
        }}
      >
        <button>Entrar</button>
        <button>Cursos</button>
        <button>Saiba mais</button>
      </aside>
    </div>
  );
}
