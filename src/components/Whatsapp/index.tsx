import React from "react";
import WhatsappSVG from "assets/img/whatsapp.svg";

import styles from "./Whatsapp.module.css";

export default function Whatsapp() {
  return (
    <button onClick={() => {}} className={styles.container}>
      <img src={WhatsappSVG} alt="Whatsapp" />
    </button>
  );
}
