import React, { useEffect, useState } from "react";
import ArrowRight from "assets/img/arrow-right.svg";

import styles from "./ScrollUp.module.css";

export default function ScrollUp() {
  const [showScrollUp, setShowScrollUp] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 50) {
        setShowScrollUp(true);
      } else {
        setShowScrollUp(false);
      }
    });
  }, []);

  const handleGoTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <>
      {showScrollUp && (
        <button onClick={handleGoTop} className={styles.container}>
          <img src={ArrowRight} alt="ScrollUp" />
        </button>
      )}
    </>
  );
}
