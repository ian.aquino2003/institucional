import { Field, Form, Formik, FormikValues } from "formik";
import React from "react";
import * as yup from "yup";

import styles from "./Newsletter.module.css";

export default function Newsletter() {
  const newsletterFormSchema = yup.object().shape({
    email: yup
      .string()
      .email("Email é inválido")
      .required("Email é necessário"),
  });
  function handleSubmitForm(values: FormikValues, evt: FormikValues) {
    console.log(values);
    evt.resetForm();
  }
  return (
    <div className={styles.container}>
      <Formik
        initialValues={{ email: "" }}
        onSubmit={handleSubmitForm}
        validationSchema={newsletterFormSchema}
      >
        {({ errors }) => (
          <Form>
            <p>Assine nossa newsletter</p>
            <>
              {errors ? <p className={styles.error}>{errors.email}</p> : null}
              <Field name="email" type="email" placeholder="E-mail" />
              <button type="submit">Enviar</button>
            </>
          </Form>
        )}
      </Formik>
    </div>
  );
}
