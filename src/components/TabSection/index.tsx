import React, { useContext } from "react";
import About from "./About";
import Contact from "./Contact";
import TabList from "./TabList";
import { TabsContext } from "contexts/TabsProvider";

import styles from "./TabSection.module.css";

export default function TabSection() {
  const { actualContent } = useContext(TabsContext);

  return (
    <section className={styles.container}>
      <aside>
        <TabList />
      </aside>
      {actualContent === "Sobre" ? <About /> : <Contact />}
    </section>
  );
}
