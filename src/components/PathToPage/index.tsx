import React from "react";
import Home from "assets/img/home.svg";
import ArrowRight from "assets/img/arrow-right.svg";

import styles from "./PathToPage.module.css";

export default function PathToPage() {
  return (
    <div className={styles.container}>
      <img src={Home} alt="Home icon" />
      <img src={ArrowRight} alt="ArrowRight icon" />
      <p>INSTITUCIONAL</p>
    </div>
  );
}
